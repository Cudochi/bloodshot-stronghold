#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <OpenXLSX.hpp>

#include "src/CSubject.hpp"
#include "src/CParameter.hpp"
#include "src/CXlsExporter.hpp"
#include "src/CFileNamesParser.hpp"
#include "src/CFileContentParser.hpp"
#include "src/CInstructionsParser.hpp"
#include "src/CException.hpp"
#include "src/CInstructionsParser.hpp"

#include "src/CCase.cpp"
#include "src/CGender.cpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); //Appel de la méthode d'inialisation de l'interface

    model = new QStringListModel(this); //Un model est un object qui content une liste. Et la listView se voit assigner le modèle.
    QStringList List;
    //Cette liste est juste là pour remplir la listView et faire apparaitre la scrollbar.
    List << "Test1" << "Test2" << "Test3" << "Test4" << "Test5" << "Test6" << "Test7" << "Test8" << "Test9" << "Test10" << "Test11" << "Test12" << "Test13" << "Test14" << "Test15" << "Test16" << "Test17" << "Test18" << "Test19" << "Test20" << "Test21" << "Test21" << "Test22" << "Test23" << "Test24" << "Test25" << "Test26" << "Test27" << "Test28" << "Test29" << "Test30" << "Test31" << "Test32" << "Test33" << "Test34" << "Test35" << "Test36" << "Test37" << "Test38" << "Test39" << "Test40";

    model->setStringList(List);
    ui->listView->setModel(model);

    //connect(ui->pushButtonBrowse, SIGNAL(clicked()), this, SLOT(openFileBrowserForFile())); //Syntaxe obsolète
    connect(ui->pushButtonBrowse, &QToolButton::clicked, this, [this]{ openFileBrowserForFile(); }); //Nouvelle syntaxe avec les lambda
    connect(ui->toolButton, &QToolButton::clicked, this, [this]{ openFileBrowserForDir(); }); //On connecte le signal clicked du bouton toolButton de l'objet this à la fonction openFileBrowserForFile().
    connect(ui->pushButton, &QPushButton::clicked, this, [this]{ exportFile(); });

    strFilePath = ""; // Ce string est déclaré dans le header, mais il a besoin d'être AU MOINS vide si on veut l'afficher.
    ui->textBrowser->setText(QString::fromStdString(strFilePath));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFileBrowserForFile()
{
    //QString qstrFileName =
    //    QFileDialog::getOpenFileName(this, "Open a file", "directoryToOpen",
    //        "Excel (*.xlsx)"); //Manière limitée d'ouvrir le gestionnaire de fichier.
    QFileDialog dialog(this);
    dialog.setNameFilter(tr("Excel (*.xlsx)"));
    QStringList fileNames;

    if(dialog.exec())
        fileNames = dialog.selectedFiles();

    if(fileNames.length() == 1)
    {
        strFilePath = fileNames.at(0).toStdString(); //Ce truc provoque un bug si on ferme la fenêtre sans un check
    }

    //std::cout << "strFilePath in openFileBrowser : " << strFilePath << std::endl;
    std::string strFilePathDisplay = "Fichier sélectionné : " + strFilePath;

    ui->textBrowser->setText(QString::fromStdString(strFilePath));
    ui->plainTextEdit->appendPlainText(QString::fromStdString(strFilePathDisplay));
}


void MainWindow::openFileBrowserForDir()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly);

    QStringList fileNames;

    if(dialog.exec())
        fileNames = dialog.selectedFiles();

    if(fileNames.length() == 1)
    {
        strDirPath = fileNames.at(0).toStdString(); //Ce truc provoque un bug si on ferme la fenêtre sans un check
    }

    std::string strFilePathDisplay = "Dossier sélectionné : " + strDirPath;

    ui->lineEdit->setText(QString::fromStdString(strDirPath));
    ui->plainTextEdit->appendPlainText(QString::fromStdString(strFilePathDisplay));
}

void MainWindow::exportFile()
{
    //noticePath = ""; // path du fichier contenant la notice
    //noticeSheetName = ""; // nom de la feuille contenant la notice
    // peut etre hardcode, pitet ?
    // De base, c'est "Notice Paramètres". De rien, MONSIEUR.
    //sourceFolderPath = ""; // path du dossier contenant les fichiers sources aka fichiers de donnees eye-tracker des sujets

    std::string extractedParamName = ""; //ckdo, c'est un string qui a chaque itération contiendra le paramètre extrait (en théorie)
    std::string noticeSheetName = ui->lineEdit_4->text().toStdString();

    std::cout << extractedParamName << std::endl;
    std::cout << strDirPath << std::endl;
    std::cout << noticeSheetName << std::endl;


    CXlsExporter* exportDoc = new CXlsExporter(); // besoin pour exporter
    CInstructionsParser* iParseur = new CInstructionsParser(); // parser de notice
    CFileNamesParser* parseur = new CFileNamesParser(); //parser pour les noms, très important
    parseur->writeAllSubjects("../../data/", exportDoc); // écrit les noms dans le fichier standardisé

    //iParseur->parseInstructionsDoc("./Demo01.xlsx", "Notice Paramètres", "../../data/", exportDoc);

    //Bonjour, chien. Si ca ne marche pas, merci de decommenter la ligne ci-dessus, et de commenter les lignes de
    // "BORNE 1" a "BORNE 2".
    //Zebi.
    // PS, si tu as une erreur, ca la resolvera probablement pas
    // JE T'EN PRIE CKDO

    // --- --- BORNE 1 --- ---
    int rowNumber = 1;

    while(iParseur->getCurrentParam()->getStrVarName() != "WARNING-EOF")
    {
        iParseur->parseInstructionsDocRow(strFilePath, noticeSheetName, strDirPath, exportDoc, rowNumber);
        extractedParamName = iParseur->getCurrentParam()->getStrVarName() + " - " + iParseur->getCurrentParam()->getStrVarNameComplement();
        rowNumber++;
    }
    // --- --- BORNE  2 --- ---
    
    std::cout << "Boutton 'Exporter' pushed." << std::endl;
}
