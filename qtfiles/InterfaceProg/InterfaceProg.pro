QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    src/CCase.cpp \
    src/CException.cpp \
    src/CFileContentParser.cpp \
    src/CFileNamesParser.cpp \
    src/CGender.cpp \
    src/CInstructionsParser.cpp \
    src/CLibrary.cpp \
    src/CParameter.cpp \
    src/CSubject.cpp \
    src/CXlsExporter.cpp \
    src/ZI.cpp

HEADERS += \
    mainwindow.h \
    src/CException.hpp \
    src/CFileContentParser.hpp \
    src/CFileNamesParser.hpp \
    src/CInstructionsParser.hpp \
    src/CParameter.hpp \
    src/CSubject.hpp \
    src/CXlsExporter.hpp

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += /home/kavan/Documents/projects/bloodshot-stronghold/lib/OpenXLSX/build/output/
INCLUDEPATH += /home/kavan/Documents/projects/bloodshot-stronghold/lib/OpenXLSX/build/library/
INCLUDEPATH += /home/kavan/Documents/projects/bloodshot-stronghold/lib/OpenXLSX/library/
INCLUDEPATH += /home/kavan/Documents/projects/bloodshot-stronghold/lib/OpenXLSX/library/headers/

# --- Static ---

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/release/ -lOpenXLSX-static
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/debug/ -lOpenXLSX-static
else:unix: LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/ -lOpenXLSX-static

INCLUDEPATH += $$PWD/../../lib/OpenXLSX/build/output
DEPENDPATH += $$PWD/../../lib/OpenXLSX/build/output

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/OpenXLSX/build/output/release/libOpenXLSX-static.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/OpenXLSX/build/output/debug/libOpenXLSX-static.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/OpenXLSX/build/output/release/OpenXLSX-static.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/OpenXLSX/build/output/debug/OpenXLSX-static.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../lib/OpenXLSX/build/output/libOpenXLSX-static.a

# --- Shared ---

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/release/ -lOpenXLSX-shared
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/debug/ -lOpenXLSX-shared
else:unix: LIBS += -L$$PWD/../../lib/OpenXLSX/build/output/ -lOpenXLSX-shared

INCLUDEPATH += $$PWD/../../lib/OpenXLSX/build/output
DEPENDPATH += $$PWD/../../lib/OpenXLSX/build/output

unix|win32: LIBS += -lstdc++fs
