#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QFileDialog>
#include <string>
#include <iostream>
#include <vector>



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots :
    void openFileBrowserForFile();
    void openFileBrowserForDir();
    void exportFile();


private:
    Ui::MainWindow *ui;
    QStringListModel *model;
    QPushButton *pushButtonBrowse;
    std::string strFilePath;
    std::string strDirPath;
    std::string noticePath;
    std::string noticeSheetName;

};
#endif // MAINWINDOW_H
