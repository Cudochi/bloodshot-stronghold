/*!
 * \file CFileNamesParser.hpp
 * \brief Parser that analyses the files name in a folder.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/

#ifndef CFILENAMESPARSERHPP
#define CFILENAMESPARSERHPP

#include <string>
#include <iostream>
#include <vector>
#include <experimental/filesystem>
#include "CSubject.hpp"


using namespace std;

/*!
 * \class CFileNamesParser
 * \brief Class parsing the source files in order to extract the parameters of the subject (id, age, case, gender) from their names.
*/
class CFileNamesParser
{
    private :
        CSubject currentSubject; //!<@brief Current CSubject object storing the extracted parameters from the filenames analyzed.
        vector<string> fileNames; //!<@brief List of filenames extracted from the analyzed folder.

    public :

        // --- --- --- SETTERS  --- --- --- //
        /*!
           \brief Sets the attributes of the currentSubject object occordindly to the fileName string.
           \param fileName String containing a filename to parse in order to get its attributes.
        */
        void setSubjectFromFilename(string fileName);

        // --- --- --- GETTERS  --- --- --- //

        /*!
           \brief Getter of currentSubject object.
           \return The CSubject object 'currentSubject'.
        */
        CSubject* getCurrentSubject();

        // --- --- --- METHODS  --- --- --- //

        /*!
           \brief Check if the content of string is a number.
           \param strSample The string to check.
           \return True if the content of the string 'strSample' can be converted to a number, else False.
        */
        bool isNumber(string strSample);

        /*!
           \brief Check wether the string subject ends with the string sample or not.
           \param subject String that may end with the parameter 'sample'.
           \param sample The string we want to determine if the subject ends with or not.
           \return True if 'subject' ends with 'sample', else False.
        */
        bool endsWith(string subject, string sample);
        /*!
           \brief Method exporting in a result file the subjects parameters of the sources files contained in the folder 'path'.
           \param path The path to extract filenames (meaning subject parameters) from.
        */
        void writeAllSubjects(string folderPath, CXlsExporter* exportDoc);
        /*!
           \brief Method returning the list of all the filenames in a given directory.
           \param folderPath The directory we want to get filenames from.
           \return A vector of strings, each string containing the name of a file from the scanned directory.
        */
        vector<string> listFilesNamesInDirectory(string folderPath);
        /*!
           \brief Method returning the list of all the filenames in a given directory corresponding to those that have to ba analyzed.
           \param folderPath The directory we want to get filenames from.
           \return A vector of strings, each string containing the name of a file from the scanned directory.
        */
        vector<string> listRelevantFilesNamesInDirectory(string folderPath);
};

#include "CFileNamesParser.cpp"

#endif
