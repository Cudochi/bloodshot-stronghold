/*!
 * \file CXlsExporter.hpp
 * \brief To export data to the .xlsx format, header file.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/

#ifndef CXLSEXPORTERHPP
#define CXLSEXPORTERHPP

#include <OpenXLSX.hpp>
#include <string>

#include "CParameter.hpp"
#include "CSubject.hpp"

using namespace std;
using namespace OpenXLSX;

/*!
 * \class CXlsExporter
 * \brief Class that is in charge of writing in the .xlsx files.
*/
class CXlsExporter
{
    // --- --- --- ATTRIBUTES --- --- --- //
    private :

    XLDocument docExport; /**< An XLDocument linked to the rseult file created in the constructor. Will be used to write all values in result file.*/

    string strFileName; //!< String containing the name of the result file.

    string strSheetName; //!< String containing the name of the active sheet of the result file.

    // --- --- --- CONSTR_DESTR --- --- --- //

    public :
    /*!
       \brief Default constructor of the CXlsExporter Class. Creates a new result file.
    */
    CXlsExporter();
    /*!
       \brief Default destructor of CXlsExporter Class. Saves and closes the result file.
    */
    ~CXlsExporter();


    // --- --- --- SETTERS --- --- --- //
    /*!
       \brief Setter of the strFileName attribute.
       \param newName String containing the new name to set.
       \pre Parameter 'newName' is a valid file name.
    */
    void setFileName(string newName);
    /*!
       \brief Setter of the strSheetName attribute.
       \param newName String containing the new name to set.
       \pre Parameter 'newName' is a valid sheet name.
    */
    void setSheetName(string newName);


    // --- --- --- GETTERS --- --- --- //

    /*!
       \brief Getter of the strFileName attribute.
       \return Value of strFileName attribute.
    */
    string getFileName();
    /*!
       \brief Getter of the strSheetName attribute.
       \return Value of strSheetName attribute.
    */
    string getSheetName();


    // --- --- --- METHODS --- --- --- //

    /*!
       \brief Method writing a integer value in the indicated cell of the result file.
       \param value Int to write in the result file.
       \param cell String indicating the cell to write in (examples : "A1", "BE54").
       \pre The 'cell' string has to be a valid cell name.
    */
    void writeInt(int value, string cellName);
    /*!
       \brief Method writing a float value in the indicated cell of the result file.
       \param value Float to write in the result file.
       \param cell String indicating the cell to write in (examples : "A1", "BE54").
       \pre The 'cell' string has to be a valid cell name.
    */
    void writeFloat(float value, string cell);
    /*!
       \brief Method writing a string value in the indicated cell of the result file.
       \param value String to write in the result file.
       \param cell String indicating the cell to write in (examples : "A1", "BE54").
       \pre The 'cell' string has to be a valid cell name.
    */
    void writeString(string value, string cell);
    /*!
       \brief Writes the following CSubject object 'newSubject' attributes in a result file : intSubId, intSubAge, subCase, subGender. Go to the first empty cell in the "A" column, and writes at this line these attributes in the given order in columns {'A', 'B', 'C', 'D'}. If the result file was considered as empty (empty cell "A1"), writes the columns names ("Id", "Age", "Case", "Gender").
       \param newSubject CSubject object to write in the result file.
    */
    void writeNewSubject(CSubject newSubject);
    /*!
       \brief Warning ! Method not done !
       \param id The id of the Subject from which the CParameter 'value' was extracted (example : Total Time Tracked of the subject number 002).
       value CParameter object to write in the result file.
    */
    void writeParam(int id, CParameter value);

    /*!
       \brief Returns the number of the line of the result file containing the subject whose id matches the id parameter. The programs stops when meeting an empty cell on the 'A' column, and then returns 0.
       \param id The Integer to find in the 'A' column of the result file. Is supposed to correspond to the id of a subject.
       \return The line with the id, 0 if not found.
    */
    unsigned int getLine(unsigned int id);

    /*!
       \brief Return the index of the first row with its cell on the 'A' column empty.
       \return Index of the first empty cell of the 'A' column, theorically corresponding to an empty row.
    */
    unsigned int getFirstEmptyLine();

    /*!
       \brief Returns the column index of the cell on the line '1' containing the string matching 'parameterName'. If not found, returns the string "0".
       \param parameterName The String to search on the line '1' of the result file.
       \return The column index of the cell containing the same string as parameterName, else the string "0" if not found.
    */
    string getColumn(string parameterName);

};

#include "CXlsExporter.cpp"

#endif
