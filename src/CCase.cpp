/*!
 * \file CCase.cpp
 * \brief Contains the represent of the possible siutation of the subject : in the group control, or on the autism spectrum (or N/A).
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/12
*/

 #ifndef CCASE
 #define CCASE

#include <vector>
#include <string>

using namespace std;

enum CCase
{
    Control,
    Patient,
    NA_Case
};

const vector<string> caseValues{"Control", "Patient", "NA"};

string caseToString(int caseIndex)
{
    return caseValues.at(caseIndex);
}


#endif
