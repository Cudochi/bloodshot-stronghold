/*!
 * \file CFileNamesParser.cpp
 * \brief Parser that analyses the files name in a folder.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/

#ifndef CFILENAMESPARSERCPP
#define CFILENAMESPARSERCPP

#include <vector>
#include <string>
#include <experimental/filesystem>
#include <sstream>
#include <iterator>

#include "CFileNamesParser.hpp"
#include "CSubject.hpp"
#include "CXlsExporter.hpp"
#include "CException.hpp"

#include "CCase.cpp"
#include "CGender.cpp"

namespace fs = std::experimental::filesystem;
using namespace std;


// --- --- --- SETTERS  --- --- --- //

void CFileNamesParser::setSubjectFromFilename(string filename) //filename : {C/P}_{id}_{age_in_months}_{gender}_{?}.xlsx
{
    CSubject returnSubject = *(new CSubject());
    try
    {
        if(!endsWith(filename, ".xlsx")) //check if the file is a .xlsx
        {
            currentSubject.clearSubject();
            throw(new CException(1, "File : \"" + filename + "\" not an .xlsx !"));
        }
    }
    catch(CException* exc)
    {
        exc->exceptionHandler();
        delete exc;
        return;
    }

    vector<string> tokens;
    string token;
    istringstream tokenStream(filename);
    while (std::getline(tokenStream, token, '_'))
    {
        tokens.push_back(token);
    }
    // use first value, the case
    try
    {
        if(tokens.size() < 4 || !isNumber(tokens.at(1)) ||  !isNumber(tokens.at(2)) )
        {
            currentSubject.clearSubject();
            throw(new CException(1, "File : \"" + filename + "\" has the wrong format !"));
        }
    }
    catch(CException* exc)
    {
        exc->exceptionHandler();
        delete exc;
        return;
    }

    if( (char)tokens.at(0).at(tokens.at(0).size() - 1) == 'C') currentSubject.setSubCase(Control);
    else if((char)tokens.at(0).at(tokens.at(0).size() - 1) == 'P') currentSubject.setSubCase(Patient);
    else currentSubject.setSubCase(NA_Case);

    currentSubject.setIntSubId(stoi(tokens.at(1)));

    currentSubject.setIntSubAge(stoi(tokens.at(2)));

    if(tokens.at(3) == "F") currentSubject.setSubGender(Female);
    else if(tokens.at(3) == "M") currentSubject.setSubGender(Male);
    else currentSubject.setSubGender(NA_Gender);
}

// --- --- --- GETTERS  --- --- --- //

CSubject* CFileNamesParser::getCurrentSubject()
{
    return &currentSubject;
}

bool CFileNamesParser::isNumber(string strSample)
{
    for (int i = 0; i < strSample.length(); i++)
    {
        if (isdigit(strSample[i]) == false) return false;
    }
    return true;
}

// --- --- --- METHODS  --- --- --- //

bool CFileNamesParser::endsWith(string subject, string sample)
{
    return subject.size() >= sample.size() && 0 == subject.compare(subject.size()-sample.size(), sample.size(), sample);
}

void CFileNamesParser::writeAllSubjects(string path, CXlsExporter* exporter)
{
    vector<string> result = listFilesNamesInDirectory(path);

    for(int i = 0; i < result.size(); i++)
    {
        setSubjectFromFilename(result.at(i));
        if(currentSubject.getIntSubId() != 0)
        {
            try
            {
                exporter->writeNewSubject(*getCurrentSubject());
            }
            catch(CException* exc)
            {
                exc->exceptionHandler();
                delete exc;
            }
        }
    }
}

vector<string> CFileNamesParser::listFilesNamesInDirectory(string folderPath)
{
    vector<string> vectListOfFiles;
    for (const auto & entry : fs::directory_iterator(folderPath))
    {
        vectListOfFiles.push_back(entry.path());
    }
    return vectListOfFiles;
}

vector<string> CFileNamesParser::listRelevantFilesNamesInDirectory(string folderPath)
{
    vector<string> vectListOfFiles = listFilesNamesInDirectory(folderPath);
    cout << "a :" << vectListOfFiles.size() << endl;
    for(int i = 0; i < vectListOfFiles.size(); i++)
    {
        if(!endsWith(vectListOfFiles.at(i), ".xlsx")) //check if the file is a .xlsx
        {
            vectListOfFiles.erase(vectListOfFiles.begin() + i);
            continue;
        }

        vector<string> tokens;
        string token;
        istringstream tokenStream(vectListOfFiles.at(i));

        while (std::getline(tokenStream, token, '_'))
        {
            tokens.push_back(token);
        }
        if(tokens.size() < 4 || !isNumber(tokens.at(1)) ||  !isNumber(tokens.at(2)) )
        {
            vectListOfFiles.erase(vectListOfFiles.begin() + i);
            continue;
        }
    }
    return vectListOfFiles;
}

#endif







//
