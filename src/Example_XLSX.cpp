#include <OpenXLSX.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace OpenXLSX;

int main()
{
    cout << "********************************************************************************\n";
    cout << "TEST PROGRAM #01: WRITING\n";
    cout << "********************************************************************************\n";

    XLDocument doc;
    doc.create("./Demo01.xlsx");
    auto wks = doc.workbook().worksheet("Sheet1");

    string cellRef = "";
    for(int i = 0; i < 50; i++)
    {
        cellRef = "A" + to_string(i);
        wks.cell(XLCellReference(cellRef)).value() = i;
    }
    doc.save();

    return 0;
}
