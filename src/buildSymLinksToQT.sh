#!/bin/bash

src_wd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $src_wd && cd ../qtfiles/InterfaceProg/src/

qt_wd=$(pwd)

#echo $cwd
#echo $qt_wd

ln -s $src_wd/CCase.cpp $qt_wd/

ln -s $src_wd/CException.cpp $qt_wd/
ln -s $src_wd/CException.hpp $qt_wd/

ln -s $src_wd/CFileContentParser.cpp $qt_wd/
ln -s $src_wd/CFileContentParser.hpp $qt_wd/

ln -s $src_wd/CFileNamesParser.cpp $qt_wd/
ln -s $src_wd/CFileNamesParser.hpp $qt_wd/

ln -s $src_wd/CGender.cpp $qt_wd/

ln -s $src_wd/CInstructionsParser.cpp $qt_wd/
ln -s $src_wd/CInstructionsParser.hpp $qt_wd/

ln -s $src_wd/CLibrary.cpp $qt_wd/

ln -s $src_wd/CParameter.cpp $qt_wd/
ln -s $src_wd/CParameter.hpp $qt_wd/

ln -s $src_wd/CSubject.cpp $qt_wd/
ln -s $src_wd/CSubject.hpp $qt_wd/

ln -s $src_wd/CXlsExporter.cpp $qt_wd/
ln -s $src_wd/CXlsExporter.hpp $qt_wd/

ln -s $src_wd/ZI.cpp $qt_wd/



