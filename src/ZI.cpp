/*!
 * \file ZI.cpp
 * \brief Contains the enumeration of the multiples zones watched by the subject. 
 * \author Kavan LEHO
 * \author François BRISON
 * \date 15/12/12
*/

#ifndef ZICPP
#define ZICPP

#include <vector>
#include <string>

enum ZI
{
    Screen,
    Item,
    Face,
    Eyes,
    Nose,
    Mouth,
    NA_ZI
};

const vector<string> ziValues{"Screen", "Item", "Face", "Eyes", "Nose", "Mouth", "NA"};

string ziToString(int ziIndex)
{
    return ziValues.at(ziIndex);
}

ZI stringToZI(string strZI)
{
    for(int i = 0; i < ziValues.size(); i++) // for each value of the enum
    {
        if(ziToString(i) == strZI) return (ZI)i; // return the index of a found ZI
    }
    return (ZI)6; // if not found, returns thz index of the NA
}

#endif
