/*!
 * \file CLibrary.cpp
 * \brief Contains a map of strings.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/12
*/
#ifndef CLIBRARYCPP
#define CLIBRARYCPP

using namespace std;
#include <map>

/*!
 * \class CLibrary
 * \brief Class that will contain the mathematical formulas used to calculate the ROC curves.
*/
class CLibrary
{
    map<string, string> dctMethodsByParam;
    //dctMethodsByParam.insert(make_pair("methodName", "variableEntered"));
};

#endif
