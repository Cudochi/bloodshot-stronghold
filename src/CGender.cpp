/**
  * \file CGender.cpp
  * \brief Contain the object that represent the gender of the subject.
  * \author Kavan LEHO
  * \author François BRISON
  * \date 06/11/12
 */

#ifndef CGENDER
#define CGENDER

#include <string>

enum CGender
{
    Male,
    Female,
    NA_Gender
};

const vector<string> genderValues{"Male", "Female", "NA"};

string genderToString(int genderIndex)
{
    return genderValues.at(genderIndex);
}
#endif
