/*!
 * \file CSubject.cpp
 * \brief Represent a subject within the program.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 02/12/20
*/
#ifndef CSUBJECTCPP
#define CSUBJECTCPP

#include <string>
#include "CCase.cpp"
#include "CGender.cpp"
#include "CSubject.hpp"

// --- --- --- CONSTR_DESTR --- --- --- //

CSubject::CSubject()
{
    clearSubject();
}

CSubject::~CSubject()
{
    clearSubject();
}

void CSubject::operator=(CSubject toCopy)
{
    setStrSubName(toCopy.getStrSubName());
    setIntSubAge(toCopy.getIntSubAge());
    setIntSubId(toCopy.getIntSubId());
    setSubCase(toCopy.getSubCase());
    setSubGender(toCopy.getSubGender());
}

// --- --- --- SETTERS --- --- --- //
void CSubject::setStrSubName(string strNewSubName)
{
    strSubName = strNewSubName;
}

void CSubject::setIntSubAge(unsigned int newVal)
{
    intSubAge = newVal;
}

void CSubject::setSubCase(CCase newCase)
{
    subCase = newCase;
}
void CSubject::setSubGender(CGender newGender)
{
    subGender = newGender;
}
void CSubject::setIntSubId(unsigned int intNewId)
{
    intSubId = intNewId;
}


// --- --- --- GETTERS --- --- --- //
string CSubject::getStrSubName()
{
    return strSubName;
}
unsigned int CSubject::getIntSubAge()
{
    return intSubAge;
}
CCase CSubject::getSubCase()
{
    return subCase;
}
CGender CSubject::getSubGender()
{
    return subGender;
}
unsigned int CSubject::getIntSubId()
{
    return intSubId;
}

// --- --- --- METHODS --- --- --- //
void CSubject::clearSubject()
{
    setStrSubName("");
    setIntSubAge(0);
    setIntSubId(0);
    setSubCase(NA_Case);
    setSubGender(NA_Gender);
}

#endif
