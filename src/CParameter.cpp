/**
  * \file CParameter.cpp
  * \brief Represent the parameters of a subject within the program.
  * \author Kavan LEHO
  * \author François BRISON
  * \date 06/11/12
*/
#ifndef CPARAMCPP
#define CPARAMCPP

#include <string>
#include "CParameter.hpp"
#include "ZI.cpp"

using namespace std;

// --- --- --- CONSTR_DESTR --- --- --- //
CParameter::CParameter()
{
    clearParam();
}

// --- --- --- SETTERS --- --- --- //
void CParameter::clearParam()
{
    resetRealVarArray();
    setStrVarName("");
    setStrCell("");
    setRealVarResult(0);
    setZiParamZI(NA_ZI);
}
void CParameter::setRealVarArray(vector<double> newVal)
{
    realValArray = newVal;
}
void CParameter::resetRealVarArray()
{
    realValArray.clear();
}
void CParameter::addValueToRealVarArray(double newVal)
{
    realValArray.push_back(newVal);
}
void CParameter::setStrVarName(string newVal)
{
    strVarName = newVal;
}
void CParameter::setRealVarResult(double newVal)
{
    realVarResult = newVal;
}
void CParameter::setZiParamZI(ZI newVal)
{
    ziParamZI = newVal;
}
void CParameter::setStrCell(string newVal)
{
    strCell = newVal;
}
void CParameter::setStrVarNameComplement(string newStrVarNameComplement)
{
    strVarNameComplement = newStrVarNameComplement;
}

// --- --- --- GETTERS --- --- --- //
vector<double> CParameter::getRealVarArray()
{
    return realValArray;
}
string CParameter::getStrVarName()
{
    return strVarName;
}
double CParameter::getRealVarResult()
{
    return realVarResult;
}
ZI CParameter::getZiParamZI()
{
    return ziParamZI;
}
string CParameter::getStrCell()
{
    return strCell;
}
string CParameter::getStrVarNameComplement()
{
    return strVarNameComplement;
}

// --- --- --- METHODS --- --- --- //


#endif
