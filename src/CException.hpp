/*!
 * \file CException.hpp
 * \brief Handles all the exceptions in the program.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/

#ifndef CEXCEPTIONHPP
#define CEXCEPTIONHPP

#include <string>

using namespace std;

/*!
 * \class CException
 * \brief Class that handles the exeptions of the program in a clean way.
*/
class CException
{
    private :
    unsigned int intPriority; //!<@brief The priority of the message.
    string strMessage; //!<@brief The message linked to the exeption raised.

    public :
    /*!
       \brief Constructor of the CException object.
       \param newPriority The priority of the new exeption.
       \param newMessage The message of the new exeption.
       \pre Something must happen in the program that legitimatly need an exception to be raised.
       \post A CException object must be raised.
       \return A constructor never return anything.
    */
    CException(unsigned int newPriority = 0, string newMessage = "");
    /*!
       \brief Copy Constructor of the CException object.
       \param exc The CException object to copy.
       \pre Something must happen in the program that legitimatly need an exception to be raised.
       \post A CException object must be raised.
       \return A constructor never return anything.
    */
    CException(CException* exc);
    /*!
       \brief A setter for the priority level of the exception.
       \param newPriority The new priority level of the exception.
    */
    void setPrority(unsigned int newPriority);
    /*!
       \brief A setter for the message of the exception.
       \param newPriority The new message of the exception.
    */
    void setMessage(string newMessage);
    /*!
       \brief A getter for the priority level.
       \return The priority level of the exception.
    */
    unsigned int getPrority();
    /*!
       \brief A getter for the message associated with the exception.
       \return The message carried of the exception.
    */
    string getMessage();
    /*!
       \brief The handler of the exceptions. The priority level affects how the handler behave.
       \brief Error level 1 : The exception's error is displayed.
       \brief Error level 2 : The exception's error is displayed.
       \brief Error level 3 : The exception's error is displayed and the program is halted.
    */
    void exceptionHandler(); // do things depending on the exc's priority, starts with displaying a message, end up terminating the process, will require link to interface
};

#include "CException.cpp"
#endif
