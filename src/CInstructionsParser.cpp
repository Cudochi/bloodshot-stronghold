/*!
 * \file CInstructionsParser.hpp
 * \brief To read the instruction in a .xlsx file and follow the coordinates it gives.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/12
*/


#ifndef CINSTRUCTIONSPARSERCPP
#define CINSTRUCTIONSPARSERCPP

#include <string>
#include <OpenXLSX.hpp>
#include <iostream>
#include <string>

#include "CInstructionsParser.hpp"
#include "ZI.cpp"

using namespace std;
using namespace OpenXLSX;

int CInstructionsParser::checkComment(string toCheck)
{
    if(toCheck[0] == '#')
    {
        if(toCheck[1] == '#') return 2;
        else return 1;
    }
    return 0;
}

CParameter* CInstructionsParser::getCurrentParam()
{
    return &currentParam;
}

void CInstructionsParser::parseInstructionsDoc(string filename, string sheetname, string path, CXlsExporter* exportDoc)
{
    // 1 - open the doc and create the result doc
    CFileContentParser* instructionsDoc = new CFileContentParser(filename, sheetname);
    instructionsDoc->openDocToRead();

    // 2 - for each line of the instructions doc
    bool endOfFile = false;
    int rowNumber = 1;

    while(getCurrentParam()->getStrVarName() != "WARNING-EOF")
    {
        parseInstructionsDocRow(filename, sheetname, path, exportDoc, rowNumber);
        rowNumber++;
    }
}
void CInstructionsParser::parseInstructionsDocRow(string filename, string sheetname, string path, CXlsExporter* exportDoc, int rowNumber)
{
    // 1 - open the doc and create the result doc
    CFileContentParser* instructionsDoc = new CFileContentParser(filename, sheetname);
    instructionsDoc->openDocToRead();
    vector<string> rowScanned;

    string tempSheet = "";

    rowScanned = instructionsDoc->readRow(rowNumber, "A", "F");

    if(checkComment(rowScanned.at(0)) == 2) // if row has to be skipped
    {
        return;
    }
    else if(rowScanned.at(3) == "") // if this is the EOF
    {
        getCurrentParam()->setStrVarName("WARNING-EOF");
        return;
    }
    // else
    if(rowScanned.at(0) != ("") && checkComment(rowScanned.at(0)) != 1) getCurrentParam()->setStrVarName(rowScanned.at(0)); // if current value is not null or has to be skipped, use the new value
    if(rowScanned.at(1) != ("") && checkComment(rowScanned.at(1)) != 1) getCurrentParam()->setZiParamZI(stringToZI(rowScanned.at(1))); // if current value is not null or has to be skipped, use the new value
    if(rowScanned.at(2) != ("") && checkComment(rowScanned.at(2)) != 1) getCurrentParam()->setStrVarNameComplement(rowScanned.at(2)); // if current value is not null or has to be skipped, use the new value
    tempSheet = rowScanned.at(3);
    if(rowScanned.at(4) != ("") && checkComment(rowScanned.at(4)) != 1) getCurrentParam()->setStrCell(rowScanned.at(4));


    // 2.3 - for each subject file
    CFileContentParser* sourceDoc = new CFileContentParser();
    CFileNamesParser* fileNameParser = new CFileNamesParser();
    vector<string> fileNameInDir = fileNameParser->listFilesNamesInDirectory(path);
    double tempValue = 0;
    for(string fileName : fileNameInDir) // for all filenames in dir
    {
        fileNameParser->setSubjectFromFilename(fileName); //set a subject attribute of fileNameParser based on the file name
        if(fileNameParser->getCurrentSubject()->getIntSubId() == 0) continue; // filename not compatible, next iteration

        // 2.3.1 -- It's a cell
        // 2.3.1.1 - read the cell

        bool hasToWrite = true; // if sheet doesn't exists, no writing has to be made
        try
        {
            //cout << "HERE : " << fileName << ", SHEET  : " << tempSheet << ", CELL : " << getCurrentParam()->getStrCell() << endl;
            if(sourceDoc->readStrCell(fileName, tempSheet, getCurrentParam()->getStrCell()) == "")
            {
                throw(new CException(1, "Cell : \"" + getCurrentParam()->getStrCell() + "\" in sheet \"" + tempSheet + "\" and file : \"" + fileName + "\" is empty !"));
            }
            else
            {
                tempValue = stod(sourceDoc->readStrCell(fileName, tempSheet, getCurrentParam()->getStrCell()));
            }
        }
        catch (CException* exc)
        {
            hasToWrite = false;
            exc->exceptionHandler();
            delete exc;
        }
        // 2.3..1.2 - write the parameter in the current param
        currentParam.resetRealVarArray();
        currentParam.addValueToRealVarArray(tempValue);

        //2.3.2.3 - write in the result sheet the parameter
        if(hasToWrite) exportDoc->writeParam(fileNameParser->getCurrentSubject()->getIntSubId(), *getCurrentParam());

        // 2.3..1.3 - write the parameter in the result sheet

        // 2.3.2 -- It's an operation


        // 2.3.3 -- It's a column
    }
}

#endif
