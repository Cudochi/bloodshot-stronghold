/*!
 * \file CException.cpp
 * \brief Handles all the exceptions in the program.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/

#ifndef CEXCEPTIONCPP
#define CEXCEPTIONCPP

#include "CException.hpp"
#include <iostream>
#include <string>

using namespace std;

CException::CException(unsigned int newPriority, string newMessage)
{
    setPrority(newPriority);
    setMessage(newMessage);
}
CException::CException(CException* exc)
{
    setPrority(exc->getPrority());
    setMessage(exc->getMessage());
}
void CException::setPrority(unsigned int newPriority)
{
    intPriority = newPriority;
}
void CException::setMessage(string newMessage)
{
    strMessage = newMessage;
}
unsigned int CException::getPrority()
{
    return intPriority;
}
string CException::getMessage()
{
    return strMessage;
}
void CException::exceptionHandler()
{
    switch (getPrority())
    {
        case 1 :
        cout << "Minor error (priority 1 out of 3)." << endl;
        cout << "\t | Error message : \n\t | " + getMessage() << endl;
        break;
        case 2 :
        cout << "Error (priority 2 out of 3)." << endl;
        cout << "\t | Error message : \n\t | " + getMessage() << endl;
        break;

        case 3 :
        cout << "Critic error (priority 3 out of 3)." << endl;
        cout << "\t | Error message : \n\t | " + getMessage() << endl;
        cout << "Program terminated." << endl;
        exit(0);
        break;

        default : // no priority
        cout << "Unknown priority ( " + to_string(getPrority()) + " )." << endl;
        cout << "\t | Error message : \n\t | " + getMessage() << endl;
    }
}

#endif
