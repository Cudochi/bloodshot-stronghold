/*!
 * \file CFileContentParser.hpp
 * \brief Parser that reads .xlsx files.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 01/12/20
*/

#ifndef CFFILECONTENTPARSERHPP
#define CFFILECONTENTPARSERHPP

#include "CFileNamesParser.hpp"

/*!
 * \class CFileContentParser
 * \brief Class that take care of parsing the .xlsx files.
*/
class CFileContentParser
{
    private :
    XLDocument docToRead; //!<@brief The object that represent the .xlsx document.
    string strSheetName; //!<@brief The name of the sheet in the doc .xlsx.
    string strFileName; //!<@brief The path or name of the .xlsx file to read.

    public :
    /*!
       \brief Contructor of CFileContentParser.
       \post An object must be created.
       \return A constructor never return anything.
    */
    CFileContentParser();
    /*!
       \brief Contructor of CFileContentParser.
       \param filename The filename that is going to be assigned to the attribute strFileName.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \post An object must be created.
       \return A constructor never return anything.
    */
    CFileContentParser(string filename);
    /*!
       \brief Contructor of CFileContentParser.
       \param filename The filename that is going to be assigned to the attribute strFileName.
       \param sheetname The sheetname that is going to be assigned to the attribute strSheetName.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \pre The sheet corresponding to the sheetname must exit.
       \post An object must be created.
       \return A constructor never return anything.
    */
    CFileContentParser(string filename, string sheetname);

    /*!
       \brief Sets the sheet name of the current CFileContentParser object.
       \param newStrSheetName The name of the sheet.
       \pre The sheet must exist.
    */
    void setStrSheetName(string newStrSheetName);
    /*!
        \brief Sets the file path of the current CFileContentParser object.
        \param newStrFileName The name of the sheet.
        \pre The file path must valid.
    */
    void setStrFileName(string newStrFileName);
    /*!
       \brief Open the .xlsx document that is in the strFileName atribute.
       \param newStrFileName The attribute strFileName must be a valid path to a .xlsx file.
    */
    void openDocToRead();
    /*!
       \brief Open the .xlsx document who's name has been passed as a parameter.
       \param newStrFileName The parameter must be a valid path to a .xlsx file.
    */
    void openDocToRead(string newStrFileName);
    /*!
       \brief Closes the current .xlsx document.
       \pre A document must be opened.
    */
    void closeDocToRead();

    /*!
       \brief Retreive the name of the sheet name stored in the current CFileContentParser object.
       \pre The attribute must not be empty.
       \return The name of the sheet.
    */
    string getStrSheetName();
    /*!
       \brief Retreive the name/path of the currently used file, stored in the current CFileContentParser object.
       \pre The attribute must not be empty.
       \return The name/path of the file.
    */
    string getStrFileName();

    /*!
       \brief Read a column from the provided start to the provided end.
       \param filename The name or the absolute path of the file to read.
       \param sheetname The name of the sheet to read.
       \param col The name of the column to scan.
       \param start The starting position of the scanner.
       \param stop The ending position of the scanner.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \pre The sheet corresponding to the sheetname must exit.
       \pre The starting position must be superior of equal to 1.
       \pre The ending position must be superior of equal to the starting position.
       \return A vector of string containing the data in each scanned cell is returned.
    */
    vector<string> readCol(string filename, string sheetname, string col, int start, int stop);
    /*!
       \brief Read a line from the provided start to the provided end.
       \param filename The name or the absolute path of the file to read.
       \param sheetname The name of the sheet to read.
       \param col The name of the column to scan.
       \param start The starting position of the scanner.
       \param stop The ending position of the scanner.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \pre The sheet corresponding to the sheetname must exit.
       \pre The starting position must be superior of equal to A (in the Excel alphanumeric system).
       \pre The ending position must be superior of equal to the starting position (in the Excel alphanumeric system).
       \return A vector of string containing the data in each scanned cell is returned.
    */
    vector<string> readRow(string filename, string sheetname, int row, string start, string stop);
    /*!
       \brief Read a line from the provided start to the provided end.
       \param col The name of the column to scan.
       \param start The starting position of the scanner.
       \param stop The ending position of the scanner.
       \pre This function uses the filename provided by the object : the attribute must contain a valid filename or filepath if the program is not executed in the same folder.
       \pre This function uses the sheetname provided by the object : the attribute must contain a valid sheetname.
       \pre The starting position must be superior of equal to A (in the Excel alphanumeric system).
       \pre The ending position must be superior of equal to the starting position (in the Excel alphanumeric system).
       \return A vector of string containing the data in each scanned cell is returned.
    */
    vector<string> readRow(int row, string start, string stop); // starts at cell  <start><row>, stops at cell <stop><row>, doc has to be opened
    /*!
       \brief Scans a single cell.
       \param cell The coordinates of the cell in the Excel format (ex : A1 ; ZZ55)
       \param sheetname The name of the sheet to read.
       \pre This function uses the filename provided by the object : the attribute must contain a valid filename or filepath if the program is not executed in the same folder.
       \pre The cell scanned must contain a float.
       \pre The cell coordinates must be written in the adequate format.
       \pre The sheet corresponding to the sheetname must exit.
       \return Return the float contained in the cell
    */
    float readCell(string sheetname, string cell);
    /*!
       \brief Scans a single cell.
       \param cell The coordinates of the cell in the Excel format (ex : A1 ; ZZ55)
       \param sheetname The name of the sheet to read.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \pre The cell scanned must contain a float.
       \pre The cell coordinates must be written in the adequate format.
       \pre The sheet corresponding to the sheetname must exit.
       \return Return the float contained in the cell
    */
    float readCell(string filename, string sheetname, string cell);
    /*!
       \brief Scans a single cell.
       \param cell The coordinates of the cell in the Excel format (ex : A1 ; ZZ55)
       \param sheetname The name of the sheet to read.
       \pre This function uses the filename provided by the object : the attribute must contain a valid filename or filepath if the program is not executed in the same folder.
       \pre The cell scanned must contain a float.
       \pre The cell coordinates must be written in the adequate format.
       \pre The sheet corresponding to the sheetname must exit.
       \return Return the float contained in the cell
    */
    string readStrCell(string sheetname, string cell);
    /*!
       \brief Scans a single cell.
       \param cell The coordinates of the cell in the Excel format (ex : A1 ; ZZ55)
       \param sheetname The name of the sheet to read.
       \pre If only the name of the file is provided, then it is assumed that the file is in the same folder where the program is executed.
       \pre The cell coordinates must be written in the adequate format.
       \pre The sheet corresponding to the sheetname must exit.
       \return Return the content of the cell
    */
    string readStrCell(string filename, string sheetname, string cell);
    /*!
       \brief Generates the next column name for iteration purposes
       \param colName Is the starting point of the generation
       \pre colName must be a valild column name.
       \post The name generated must be the immediate follower of the one passed as an argument.
       \return Returns the next column name.
    */
    string columnNameGenerator(string colName); // Generates a list of column names for iteration purposes

};

#include "CFileContentParser.cpp"

#endif
