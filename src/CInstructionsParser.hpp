/*!
 * \file CInstructionsParser.hpp
 * \brief To read the instruction in a .xlsx file and follow the coordinates it gives.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/12
*/

#ifndef CINSTRUCTIONSPARSERHPP
#define CINSTRUCTIONSPARSERHPP

#include <string>
#include <iostream>
#include "CParameter.hpp"
#include "CFileContentParser.hpp"

using namespace std;

/*!
 * \class CInstructionsParser
 * \brief Class that contain the parser for the inscructions in the .xlsx files.
*/
class CInstructionsParser
{
    private :
        CParameter currentParam;
    public :
        CParameter* getCurrentParam();

        int checkComment(string toCheck); // returns 0 if no #, 1 if 1 #, 2 for 2#
        int checkComment(string filename, string sheetname, string cell);
        int checkCommentLine(string toCheck);
        int checkCommentLine(string filename, string sheetname, string row);

        void parseInstructionsDoc(string filename, string sheetname, string path, CXlsExporter* exportDoc); // parses all the doc
        void parseInstructionsDocRow(string filename, string sheetname, string path, CXlsExporter* exportDoc, int row); // parses all the doc
        vector<string> getLine(int lineNumber); // get a line, used during the parsing
};

#include "CInstructionsParser.cpp"

#endif
