/**
  * \file CParameter.hpp
  * \brief Represent the parameters of a subject within the program.
  * \author Kavan LEHO
  * \author François BRISON
  * \date 06/11/12
*/

#ifndef CPARAMHPP
#define CPARAMHPP

#include <vector>
#include <string>
#include "ZI.cpp"


using namespace std;

/*!
 * \class CParameter
 * \brief Class that represent the parameters of a subject within the program.
*/
class CParameter
{
    private :
        vector<double> realValArray; //!<@brief Array of values scanned in the .xlsx document of the subject.
        string strVarName; //!<@brief Name of the values contained by the attribute realValArray.
        string strVarNameComplement; //!<@brief Complementary part of the name of the values contained by the attribute realValArray.
        string strCell; //!<@brief Cell where the parameter has to be read.
        double realVarResult; //!<@brief The value computed from the array realValArray.
        ZI ziParamZI; //!<@brief A ZI object to specify the zones that have been focused on.

    public :
        /*!
           \brief Contructor of CParameter.
           \post An object must be created.
           \return A constructor never return anything.
        */
        CParameter();

        /*!
           \brief A setter for the attribute realValArray.
           \param newRealValArray The new value of realValArray.
           \pre The data in the parameter must be valid and exploitable.
           \post The data in the attribute must be valid and exploitable.
        */
        void setRealVarArray(vector<double> newRealValArray);
        /*!
           \brief Add a value at the end of vector of double realValArray.
           \param newVal The new value to add.
           \post The data contained in realValArray must be coherent.
        */
        void addValueToRealVarArray(double newVal);
        /*!
           \brief Clear the content of realValArray.
           \post realValArray Must be empty.
        */
        void resetRealVarArray();
        /*!
           \brief Clear all the attributes of the CParameter object.
           \post All the attribute of the CParameter object must be empty.
        */
        void clearParam();
        /*!
           \brief A getter for the realValArray attribute.
           \return Returns realValArray under the form of a vector<double>.
        */
        vector<double> getRealVarArray();

        /*!
           \brief A setter for the attribute strVarName.
           \param newStrVarName The new value of strVarName.
        */
        void setStrVarName(string newStrVarName);
        /*!
           \brief A getter for the attribute strVarName.
           \return The value of strVarName.
        */
        string getStrVarNameComplement();

        /*!
           \brief A setter for the attribute strVarNameComplement.
           \param newStrVarNameComplement The new value of strVarNameComplement.
        */
        void setStrVarNameComplement(string newStrVarNameComplement);
        /*!
           \brief A getter for the attribute strVarNameComplement.
           \return The value of strVarName.
        */
        string getStrVarName();

        /*!
           \brief A setter for the attribute strCell.
           \param newStrCell The new value of strCell.
        */
        void setStrCell(string newStrCell);
        /*!
           \brief A getter for the attribute strCell.
           \return The value of strCell.
        */
        string getStrCell();

        /*!
           \brief A setter for the attribute realVarResult.
           \param newRealVarResult The new value of realVarResult.
        */
        void setRealVarResult(double newRealVarResult);
        /*!
           \brief A getter for the attribute realVarResult.
           \return The value of realVarResult.
        */
        double getRealVarResult();

        /*!
           \brief A setter for the attribute ziParamZI.
           \param newZiParamZI The new value of ziParamZI.
        */
        void setZiParamZI(ZI newZiParamZI);
        /*!
           \brief A getter for the attribute ziParamZI.
           \return The ZI object ziParamZI.
        */
        ZI getZiParamZI();
};

#include "CParameter.cpp"
#endif
