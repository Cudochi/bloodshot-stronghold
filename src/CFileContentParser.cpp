/*!
 * \file CFileContentParser.cpp
 * \brief Parser that reads .xlsx files.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 01/12/20
*/

#ifndef CFFILECONTENTPARSERCPP
#define CFFILECONTENTPARSERCPP

#include <vector>
#include "CFileContentParser.hpp"
#include "CException.hpp"

using namespace std;

// --- --- --- CONSTR_DESTR --- --- --- //

CFileContentParser::CFileContentParser()
{}

CFileContentParser::CFileContentParser(string filename)
{
    setStrFileName(filename);
    //create xlsx document, is not closed
}


CFileContentParser::CFileContentParser(string filename, string sheetname)
{
    setStrFileName(filename);
    //create xlsx document, is not closed
    setStrSheetName(sheetname);
}

// --- --- --- SETTERS --- --- --- //

void CFileContentParser::setStrFileName(string newName)
{
    strFileName = newName;
}

void CFileContentParser::setStrSheetName(string newName)
{
    strSheetName = newName;
}

void CFileContentParser::openDocToRead()
{
    try
    {
        docToRead.open(getStrFileName());
    }
    catch (...)
    {
        throw(new CException(1, "Failed to open : \"" + getStrFileName() + "\". File does not exist !"));
    }
}

void CFileContentParser::openDocToRead(string newStrFileName)
{
    CFileNamesParser parseur;
    vector<string> vectListOfFiles = parseur.listFilesNamesInDirectory(".");
    for (string interFilename: vectListOfFiles)
    {
        if(interFilename == newStrFileName)
        {
            setStrFileName(newStrFileName);
            docToRead.open(getStrFileName());
            return;
        }
    }
    throw(new CException(1, "Failed to open : \"" + newStrFileName + "\". File does not exist !"));
}

void CFileContentParser::closeDocToRead()
{
    docToRead.close();
}



// --- --- --- GETTERS --- --- --- //

string CFileContentParser::getStrFileName()
{
    return strFileName;
}

string CFileContentParser::getStrSheetName()
{
    return strSheetName;
}


// --- --- --- METHODS --- --- --- //

vector<string> CFileContentParser::readCol(string filename, string sheetname, string col, int start, int stop)
{
    openDocToRead(filename);
    setStrSheetName(sheetname);
    vector<string>returnStrVect = *(new vector<string>());
    if(!docToRead.workbook().sheetExists(getStrSheetName()))
    {
        throw(new CException(1, "Sheet named : \"" + sheetname + "\" does not exist !"));
    }
    for(int i = start; i <= stop; i++)
    {
        try
        {
            returnStrVect.push_back(readStrCell(sheetname, col + to_string(i)));
        }
        catch (CException* exc)
        {
            throw(new CException(exc));
            delete exc;
        }
    }
    closeDocToRead();
    return returnStrVect;
}

vector<string> CFileContentParser::readRow(string filename, string sheetname, int row, string start, string stop)
{
vector<string> returnStrVect;
    try
    {
        openDocToRead(filename);
        setStrSheetName(sheetname);
        returnStrVect = readRow(row, start, stop);
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }
    closeDocToRead();
    return returnStrVect;
}

vector<string> CFileContentParser::readRow(int row, string start, string stop)
{
    if(!docToRead.workbook().sheetExists(getStrSheetName()))
    {
        throw(new CException(1, "Failed to open : \"" + getStrSheetName() + "\". Sheet does not exist in file \"" + getStrFileName() + "\" !"));
    }
    vector<string> returnStrVect;
    stop = columnNameGenerator(stop);
    char last = ' ';
    int i = 0;
    while(start != stop)
    {
        try
        {
            returnStrVect.push_back(readStrCell(getStrSheetName(), start + to_string(row)));
        }
        catch (CException* exc)
        {
            throw(new CException(exc));
            delete exc;
        }
        start = columnNameGenerator(start);
    }
    return returnStrVect;
}

float CFileContentParser::readCell(string filename, string sheetname, string cell) //read a call from a not opened file
{
    float returnVal;
    setStrFileName(filename);
    docToRead.open(getStrFileName());
    try
    {
        returnVal = readCell(sheetname, cell);
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }
    docToRead.close();
    return returnVal;
}

float CFileContentParser::readCell(string sheetname, string cell) // read a cell from an already opened file, need to open file first
{
    float returnVal = 0;
    setStrSheetName(sheetname);
    if(!docToRead.workbook().worksheetExists(getStrSheetName()))
    {
        throw(new CException(1, "Failed to open : \"" + getStrSheetName() + "\". Sheet does not exist in file \"" + getStrFileName() + "\" !"));
    }
    auto wks = docToRead.workbook().worksheet(getStrSheetName());
    if(wks.cell(cell).valueType() == XLValueType::Float)
    {
        returnVal = wks.cell(cell).value().get<float>();
    }
    else if (wks.cell(cell).valueType() == XLValueType::Integer)
    {
        returnVal = (float)wks.cell(cell).value().get<int>();
    }
    else
    {
        throw(new CException(1, "Wrong value type at : \"" + getStrFileName() + "\", cell : \"" + cell + "\", expected Float.\"" + getStrFileName() + "\" !"));
    }
    return returnVal;
}

string CFileContentParser::readStrCell(string filename, string sheetname, string cell) //read a call from a not opened file
{
    string returnStr = "";
    setStrFileName(filename);
    try
    {
        docToRead.open(getStrFileName());
        returnStr = readStrCell(sheetname, cell);
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }
    docToRead.close();
    return returnStr;
}

string CFileContentParser::readStrCell(string sheetname, string cell) //read a call from an already opened file
{
    setStrSheetName(sheetname);
    if(!docToRead.workbook().worksheetExists(getStrSheetName()))
    {
        throw(new CException(1, "Failed to open : \"" + getStrSheetName() + "\". Sheet does not exist in file \"" + getStrFileName() + "\" !"));
        return "0";
    }
    auto wks = docToRead.workbook().worksheet(getStrSheetName());
    return wks.cell(cell).value().asString();
}

string CFileContentParser::columnNameGenerator(string colName) // Generates a the next column name from colName
{
    int i = 0;
    bool fullZ = true;
    char last = ' ';
    i = colName.size() - 1;

    if(colName.at(i) != 'Z')
    {
        last = (char)((int)(colName.back()) + 1);
        colName.pop_back();
        colName += last;
        fullZ = false;
    }
    else
    {
        while(i >= 0)
        {
            if(colName.at(i) != 'Z')
            {
                last = (char)((int)(colName.at(i)) + 1);
                colName.at(i) = last;
                fullZ = false;
                break;
            }
            else
            {
                colName.at(i) = 'A';
                i--;
            }
        }
    }
    if(fullZ)
    {
        colName += "A";
    }
    return colName;
}

#endif
