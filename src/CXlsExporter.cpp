/*!
 * \file CXlsExporter.cpp
 * \brief To export data to the .xlsx format, content file.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 06/11/20
*/
#ifndef CXLSEXPORTERCPP
#define CXLSEXPORTERCPP

#include <string>
#include <OpenXLSX.hpp>
#include <iostream>
#include <string>
#include <ctime>

#include "CXlsExporter.hpp"
#include "CParameter.hpp"
#include "CSubject.hpp"
#include "CFileContentParser.hpp"

using namespace std;
using namespace OpenXLSX;

// --- --- --- CONSTR_DESTR --- --- --- //


CXlsExporter::CXlsExporter()
{
    //get time to create a filename
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    setFileName("./Fichier_Result_Test" + to_string(timePtr->tm_mday) + "_" + to_string(timePtr->tm_mon + 1) + "_" + to_string(timePtr->tm_year + 1900) + "_" + ".xlsx");
    //create xlsx document, is not closed
    docExport.create(getFileName());

    setSheetName("Sheet1");
    // create worksheet, can be reopened later with the same line, if the name is already existing
    auto wks = docExport.workbook().worksheet(getSheetName());
}

CXlsExporter::~CXlsExporter()
{
    docExport.save();
    docExport.close();
}

// --- --- --- SETTERS --- --- --- //

void CXlsExporter::setFileName(string newName)
{
    strFileName = newName;
}
void CXlsExporter::setSheetName(string newName)
{
    strSheetName = newName;
}


// --- --- --- GETTERS --- --- --- //

string CXlsExporter::getFileName()
{
    return strFileName;
}
string CXlsExporter::getSheetName()
{
    return strSheetName;
}

// --- --- --- METHODS --- --- --- //

void CXlsExporter::writeInt(int value, string cellName)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());
    wks.cell(XLCellReference(cellName)).value() = value;
    docExport.save();
}

void CXlsExporter::writeFloat(float value, string cellName)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());
    wks.cell(XLCellReference(cellName)).value() = to_string(value);
    docExport.save();
}

void CXlsExporter::writeString(string value, string cellName)
{
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    wks.cell(XLCellReference(cellName)).value() = value;
    docExport.save();
}

void CXlsExporter::writeNewSubject(CSubject newSubject)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted !"));
    }
    auto wks = docExport.workbook().worksheet(getSheetName());
    try
    {
        int line = getFirstEmptyLine();
        if(line == 1)
        {
            line++;
            writeString("Id", "A1");
            writeString("Age", "B1");
            writeString("Case", "C1");
            writeString("Gender", "D1");
        }
        writeInt((int) newSubject.getIntSubId(), "A" + to_string(line));
        writeInt((int) newSubject.getIntSubAge(), "B" + to_string(line));
        writeString(caseToString(newSubject.getSubCase()), "C" + to_string(line));
        writeString(genderToString(newSubject.getSubGender()), "D" + to_string(line));
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }

}


void CXlsExporter::writeParam(int id, CParameter value)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    auto wks = docExport.workbook().worksheet(getSheetName());
    int rowIndex = getLine(id);
    string column = "";
    try
    {
        column = getColumn(value.getStrVarName() + " - " + value.getStrVarNameComplement());
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }
    writeFloat(value.getRealVarArray().at(0), column + to_string(rowIndex));
    docExport.save();
}


unsigned int CXlsExporter::getLine(unsigned int id)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());

    string cellName = "A";
    int i = 2;

    while(wks.cell(cellName + to_string(i)).valueType() != XLValueType::Empty) // check the id column until an empty cell is found
    {
        if(stoi(wks.cell(cellName + to_string(i)).value().asString()) == id) return i; // if the value is the one we look for, return the line
        i++;
    }
    return 0;
}

unsigned int CXlsExporter::getFirstEmptyLine()
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());

    string cellName = "A";
    int i = 1;

    while(wks.cell(cellName + to_string(i)).valueType() != XLValueType::Empty) // check the id column until an empty cell is found
    {
        i++;
    }
    return i; //returns the line
}

string CXlsExporter::getColumn(string parameterName)
{
    if(!docExport.workbook().worksheetExists(getSheetName()))
    {
        throw(new CException(3, "Result file not existing ! Error must have occured during creation, or the file \"" + getFileName() +"\" was deleted"));
    }
    CFileContentParser* cfcp = new CFileContentParser();
    // open workspace
    auto wks = docExport.workbook().worksheet(getSheetName());

    string letters = "A";
    char last;

    while(wks.cell(letters + to_string(1)).valueType() != XLValueType::Empty) // check the id column until an empty cell is found
    {
        if(wks.cell(letters + to_string(1)).value().get<string>() == parameterName) // the parameter name we look for is found
        {
            return letters;
        }
        letters = cfcp->columnNameGenerator(letters); // "increment" the column index, fo rexample from 'A' to 'B' or from "AGF" to "AGG".
    }
    try
    {
        writeString(parameterName, letters + "1");
    }
    catch (CException* exc)
    {
        throw(new CException(exc));
        delete exc;
    }
    return letters;
}


#endif
