/**
  *\file Main.cpp
  * \brief Main file of the program.
  * \author Kavan LEHO
  * \author François BRISON
  * \date 05/11/20
 */

#include "CSubject.hpp"
#include "CParameter.hpp"
#include "CXlsExporter.hpp"
#include "CFileNamesParser.hpp"
#include "CFileContentParser.hpp"
#include "CInstructionsParser.hpp"
#include "CException.hpp"
#include "CInstructionsParser.hpp"

#include "CCase.cpp"
#include "CGender.cpp"

#include <iostream>
#include <string>
#include <vector>

#include <OpenXLSX.hpp>

using namespace std;

int main(int argc, char **argv)
{
    string noticePath = "./Demo01.xlsx"; // path du fichier contenant la notice
    string noticeSheetName = "Notice Paramètres"; // nom de la feuille contenant la notice
    // peut etre hardcode, pitet ?
    string sourceFolderPath = "../../data/"; // path du dossier contenant les fichiers sources aka fichiers de donnees eye-tracker des sujets

    string extractedParamName = ""; //ckdo, c'est un string qui a chaque itération contiendra le paramètre extrait (en théorie)

    CXlsExporter* exportDoc = new CXlsExporter(); // besoin pour exporter
    CInstructionsParser* iParseur = new CInstructionsParser(); // parser de notice
    CFileNamesParser* parseur = new CFileNamesParser();

    try
    {
        parseur->writeAllSubjects("../../data/", exportDoc);

        int rowNumber = 1;

        while(iParseur->getCurrentParam()->getStrVarName() != "WARNING-EOF")
        {
            iParseur->parseInstructionsDocRow(noticePath, noticeSheetName, sourceFolderPath, exportDoc, rowNumber);
            extractedParamName = iParseur->getCurrentParam()->getStrVarName() + " - " + iParseur->getCurrentParam()->getStrVarNameComplement();
            rowNumber++;
        }
    }
    catch(CException* exc)
    {
        exc->exceptionHandler();
        delete exc;
    }

    delete iParseur;

    std::cout << "--- Execution completed ---" << std::endl;
    return 0;
}
