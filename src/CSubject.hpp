/*!
 * \file CSubject.hpp
 * \brief Represent a subject within the program.
 * \author Kavan LEHO
 * \author François BRISON
 * \date 02/12/20
*/

#ifndef CSUBJECTHPP
#define CSUBJECTHPP

#include <string>
#include "CCase.cpp"
#include "CGender.cpp"

using namespace std;

/*!
 * \class CSubject
 * \brief Class that represent a subject.
*/
class CSubject
{
    private :
        string strSubName; //!<@brief The name of the subject.
        CCase subCase; //!<@brief Wether or not the subject is part of the control group. It is an object containing an enum.
        CGender subGender; //!<@brief Gender of the subject (male, female or N/A). It is an object containing an enum.
        unsigned int intSubAge; //!<@brief Age of the subject.
        unsigned int intSubId; //!<@brief ID of the subject.

    public :
        /*!
           \brief Contructor of CSubject.
           \post A CSubject object must be created.
           \return A constructor never return anything.
        */
        CSubject();
        /*!
           \brief Destructor of CSubject.
           \pre A CSubject object must exist.
           \post A CSubject object must be destroyed.
           \return A destructor never return anything.
        */
        ~CSubject();

        /*!
           \brief A setter for the subject name.
           \param strNewSubName The new subject name.
        */
        void setStrSubName(string strNewSubName);
        /*!
           \brief A getter for the subject name.
           \return The subject name.
        */
        string getStrSubName();

        /*!
           \brief A setter for the subject age.
           \param intNewSubAge The new age of the subject.
        */
        void setIntSubAge(unsigned int intNewSubAge);
        /*!
           \brief A getter for the subject age.
           \return The age of the subject.
        */
        unsigned int getIntSubAge();

        /*!
           \brief A setter for the ID of the subject.
           \param intNewId The new ID of the subject.
        */
        void setIntSubId(unsigned int intNewId);
        /*!
           \brief A getter for the ID of the subject.
           \return The ID of the subject.
        */
        unsigned int getIntSubId();

        /*!
           \brief A setter for the gender of the subject.
           \param intNewId The new gender of the subject. It is an object containing an enum.
        */
        void setSubGender(CGender newGender);
        /*!
           \brief A getter for the gender of the subject.
           \return The CGender object representing the current gender of the subject.
        */
        CGender getSubGender();

        /*!
           \brief A setter for the situation of the susbject.
           \param newCase The new situation of the subject. It is an object containing an enum.
        */
        void setSubCase(CCase newCase);
        /*!
           \brief A getter for the situation of the subject.
           \return The CCase object representing the current case of the subject.
        */
        CCase getSubCase();

        /*!
           \brief An overload of the = operator to allow a copy Contructor to exist.
        */
        void operator=(CSubject subjectToCopy);

        /*!
           \brief A function to clear the informations of a subject.
        */
        void clearSubject();
};

#include "CSubject.cpp"
#endif
