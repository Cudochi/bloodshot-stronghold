#include <OpenXLSX.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace OpenXLSX;

int main()
{
    cout << "********************************************************************************\n";
    cout << "TEST PROGRAM #01: WRITING\n";
    cout << "********************************************************************************\n";

    XLDocument doc;
    doc.create("./Demo01.xlsx");
    auto wks = doc.workbook().worksheet("Sheet1");

    wks.cell(XLCellReference("A1")).value() = 3.14159;
    wks.cell(XLCellReference("B1")).value() = 42;
    wks.cell(XLCellReference("C1")).value() = "  Hello OpenXLSX!  ";
    wks.cell(XLCellReference("D1")).value() = true;
    wks.cell(XLCellReference("E1")).value() = wks.cell(XLCellReference("C1")).value();

    String cellRef = "";
    for(int i = 0; i < 50; i++)
    {
        cellRef = "A" + to_string(i);
        wks.cell(XLCellReference(cellRef)).value() = i;
    }
    doc.save();

    return 0;
}
